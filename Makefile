all: main

main: main.c
	gcc $^ -o $@

run: main
	./main

clean:
	rm -f main

.PHONY: clean run
