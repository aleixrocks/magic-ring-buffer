/*
	Copyright (C) 2020 Barcelona Supercomputing Center (BSC)
*/

#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int fd;
	void *addr;
	void *addr_part;

	size_t base_size = 4096; // multiple of page size
	size_t mirror_size = base_size*2; // we will mirror the entire buffer

	// get a file descriptor of an unnamed temporary file not backed by any
	// storage device. O_TMPFILE is available since Linux kernel 3.11 (see
	// "man 2 open")
	fd = open("/tmp", O_TMPFILE | O_RDWR, 0600);
	if (fd == -1) {
		perror("Error: open\n");
		exit(EXIT_FAILURE);
	}
	// allocate backing physical memory for the buffer
	if (ftruncate(fd, base_size)) {
		perror("Error: ftruncate\n");
		exit(EXIT_FAILURE);
	}

	// allocate virtual addresses for the ring buffer (2x requestes buffer size)
	//
	// this reserves 2 * base_address contiguous virtual addresses. This is
	// necessary to ensure that there will be no allocations between the
	// first and second mmaps that could potentially overlapp.
	addr = mmap(NULL, mirror_size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS,
		   -1, 0);
	if (addr == MAP_FAILED) {
		perror("Error: mmap: base\n");
		exit(EXIT_FAILURE);
	}

	// mmap physical pages to virtual addresses
	//
	// This creates a fixed mapping (MAP_FIXED) on the first half of our
	// previous mmap. This map replaces the previous overlapping mapping
	// with the new one (see "man mmap")
	addr_part = mmap(addr, base_size,
			 PROT_READ | PROT_WRITE,
			 MAP_SHARED | MAP_FIXED, fd, 0);
	if (addr_part != addr) {
		perror("Error: mmap: part1\n");
		exit(EXIT_FAILURE);
	}
	// This creates a fixed mapping on the second half of our previous mmap.
	addr_part = mmap(((char *) addr) + base_size, base_size,
			 PROT_READ | PROT_WRITE,
			 MAP_SHARED | MAP_FIXED, fd, 0);
	if (addr_part != ((char *) addr + base_size)) {
		perror("Error: mmap: part2\n");
		exit(EXIT_FAILURE);
	}
	// it's safe to close the file descriptor as the mmaped areas keep the
	// memory alive.
	close(fd);

	// mapping done! now let's validate it!
	//
	// Writings to the first half are mirrored to the second half and vice
	// versa. This comes in handy for circular buffers.

	size_t i;

	// check first half
	for (i = 0; i < base_size; i++)
		((char *) addr)[i] = 66;
	for (i = base_size; i < mirror_size; i++)
		if (((char *) addr)[i] != 66)
			fprintf(stderr, "Error: validation: part1: byte %zu\n", i);

	// check second half
	for (i = base_size; i < mirror_size; i++)
		((char *) addr)[i] = 99;
	for (i = 0; i < base_size; i++)
		if (((char *) addr)[i] != 99)
			fprintf(stderr, "Error: validation: part2: byte %zu\n", i);

	// unmap/deallocate memory
	if (munmap(addr, mirror_size)) {
		perror("Error: munmap\n");
		exit(EXIT_FAILURE);
	}

	return 0;
}
